<?php

namespace Neon\News\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Neon\Attributable\Models\Traits\Attributable;
use Neon\Models\Traits\Uuid;
use Neon\Models\Traits\Publishable;
use Neon\Models\Traits\Statusable;
use Neon\Models\Basic as BasicModel;
use Neon\Site\Models\Traits\SiteDependencies;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Tags\HasTags;


class News extends BasicModel implements HasMedia
{
  use Attributable;
  use HasTags;
  use InteractsWithMedia;
  use LogsActivity;
  use Publishable;
  use SiteDependencies;
  use SoftDeletes;
  use Statusable;
  use Uuid; // N30N UUID to forget auto increment stuff.

  const MEDIA_HEADER  = 'news_header';
  const MEDIA_CONTENT = 'news_content';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title',
    'slug',
    'lead',
    'content',
    'status',
    'pinned',
    'published_at',
    'expired_at'
  ];

  protected $casts = [
    'pinned'        => 'boolean',
    'title'         => 'string',
    'slug'          => 'string',
    'lead'          => 'string',
    'content'       => 'string',
    'created_at'    => 'datetime',
    'updated_at'    => 'datetime',
    'deleted_at'    => 'datetime',
    'published_at'  => 'datetime',
    'expired_at'    => 'datetime'
  ];

  public function getActivitylogOptions(): LogOptions
  {
    return LogOptions::defaults();
  }

  public function registerMediaCollections(): void
  {
    $this->addMediaCollection(self::MEDIA_HEADER)->singleFile();
    $this->addMediaCollection(self::MEDIA_CONTENT);
  }

  public function registerMediaConversions(Media $media = null): void
  {
    $this->addMediaConversion('thumb')
      ->height(100)
      ->fit(\Spatie\Image\Manipulations::FIT_MAX, 100, 100)
      ->optimize()
      ->performOnCollections(self::MEDIA_HEADER, self::MEDIA_CONTENT);

    $this->addMediaConversion('medium')
      ->height(600)
      ->fit(\Spatie\Image\Manipulations::FIT_MAX, 600, 600)
      ->optimize()
      ->performOnCollections(self::MEDIA_HEADER, self::MEDIA_CONTENT);
  }

}
